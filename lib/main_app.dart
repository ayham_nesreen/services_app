import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

import 'core/helpers/screen_manager.dart';
import 'core/navigation/routes/routes_generator.dart';
import 'core/navigation/routes/routes_manager.dart';
import 'core/resources/strings_manager.dart';
import 'core/ui/theme/theme_manager.dart';

class MainApp extends StatefulWidget {
  const MainApp({super.key});

  @override
  State<MainApp> createState() => _MainAppState();
}

class _MainAppState extends State<MainApp> {
  @override
  Widget build(BuildContext context) {
    ScreenManager(context);
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      child: MaterialApp(
        onGenerateRoute: RoutesGenerator.getRoute,
        initialRoute: RoutesManager.splash,
        theme: ThemeManager.getTheme(),
        debugShowCheckedModeBanner: false,
        localizationsDelegates: context.localizationDelegates,
        supportedLocales: context.supportedLocales,
        locale: context.locale,
        title: StringsManager.appName.tr(),
      ),
    );
  }
}
