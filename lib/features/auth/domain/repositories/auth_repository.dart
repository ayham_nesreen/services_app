import 'package:dartz/dartz.dart';
import 'package:services_app/core/structure_utils/errors/failures.dart';
import 'package:services_app/features/auth/data/models/user_model.dart';
import 'package:services_app/features/auth/domain/params/login_params.dart';

abstract class AuthRepository {
  Future<Either<Failure, UserModel>> loginUser(LoginParams params);
}
