import 'package:dartz/dartz.dart';
import 'package:services_app/core/structure_utils/errors/failures.dart';
import 'package:services_app/core/structure_utils/usecases/usecase.dart';
import 'package:services_app/features/auth/data/models/user_model.dart';
import 'package:services_app/features/auth/domain/params/login_params.dart';
import 'package:services_app/features/auth/domain/repositories/auth_repository.dart';

class LoginUseCase implements UseCase<UserModel, LoginParams> {
  final AuthRepository repository;
  LoginUseCase(this.repository);
  @override
  Future<Either<Failure, UserModel>> call(LoginParams params) {
    return repository.loginUser(params);
  }
}
