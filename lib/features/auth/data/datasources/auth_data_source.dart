import 'package:services_app/features/auth/domain/params/login_params.dart';

import '../models/user_model.dart';

abstract class AuthDataSource {
  Future<UserModel> login(LoginParams params);
}
