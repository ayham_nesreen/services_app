import 'package:services_app/core/api/urls_manager.dart';
import 'package:services_app/core/enums/http_method.dart';
import 'package:services_app/features/auth/data/models/user_model.dart';

import 'package:services_app/features/auth/domain/params/login_params.dart';

import 'auth_data_source.dart';

import '../../../../core/api/request_manager.dart';

class AuthDataSourceImpl extends AuthDataSource {
  final RequestManager requestManager;

  AuthDataSourceImpl({required this.requestManager});

  @override
  Future<UserModel> login(LoginParams params) async {
    final response = await requestManager.sendRequest(
      method: HttpMethod.POST,
      url: UrlsManager.login,
      data: params.toJson(),
    );
    return UserModel.fromJson(response);
  }
}
