import 'package:dartz/dartz.dart';
import 'package:services_app/core/helpers/helper_functions.dart';

import 'package:services_app/core/structure_utils/errors/failures.dart';

import 'package:services_app/features/auth/data/models/user_model.dart';

import 'package:services_app/features/auth/domain/params/login_params.dart';

import '../../../../core/structure_utils/network/network_info.dart';
import '../../domain/repositories/auth_repository.dart';

import '../datasources/auth_data_source.dart';

class AuthRepositoryImpl implements AuthRepository {
  final AuthDataSource authDataSource;

  final NetworkInfo networkInfo;

  AuthRepositoryImpl({
    required this.authDataSource,
    required this.networkInfo,
  });

  @override
  Future<Either<Failure, UserModel>> loginUser(LoginParams params) {
    return HelperFunctions.tryToExecute<UserModel>(
        networkInfo: networkInfo, execute: () => authDataSource.login(params));
  }
}
