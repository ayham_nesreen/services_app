part of 'auth_bloc.dart';

class AuthState {}

class AuthInitial extends AuthState {}

class LoginLoading extends AuthState {}

class LoginFailed extends AuthState {
  final String msg;
  LoginFailed({required this.msg});
}

class LoginSuccessed extends AuthState {
  final UserModel userModel;
  LoginSuccessed({required this.userModel});
}
