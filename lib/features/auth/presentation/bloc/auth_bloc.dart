import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:services_app/core/extentions/failure_mapper.dart';
import 'package:services_app/features/auth/data/models/user_model.dart';
import 'package:services_app/features/auth/domain/params/login_params.dart';
import 'package:services_app/features/auth/domain/usecases/login_use_case.dart';

part 'auth_event.dart';
part 'auth_state.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  LoginUseCase loginUseCase;
  AuthBloc({required this.loginUseCase}) : super(AuthInitial()) {
    on<AuthEvent>((event, emit) async {
      if (event is LoginEvent) {
        emit(LoginLoading());
        (await loginUseCase.call(event.params)).fold(
            (l) => emit(LoginFailed(msg: l.toErrorMessage())),
            (r) => emit(LoginSuccessed(userModel: r)));
      }
    });
  }
}
