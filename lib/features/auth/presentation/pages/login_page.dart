import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:services_app/core/preferences/app_preferences.dart';
import 'package:services_app/core/ui/overlay_widget_manager/snack_bar_manager.dart';
import 'package:services_app/core/ui/widgets/loading_indicator.dart';
import 'package:services_app/core/validators/required_validator.dart';
import 'package:services_app/di/injection_container.dart';
import 'package:services_app/features/auth/domain/params/login_params.dart';
import 'package:services_app/features/auth/presentation/bloc/auth_bloc.dart';

import '../../../../core/helpers/screen_manager.dart';
import '../../../../core/navigation/routes/routes_manager.dart';
import '../../../../core/resources/strings_manager.dart';
import '../../../../core/resources/svg_manager.dart';
import '../../../../core/ui/widgets/custom_text_field.dart';
import '../../../../core/ui/widgets/simple_app_bar.dart';
import '../widgets/text_with_clickable_part.dart';
import '../widgets/login_footer_widget.dart';
import '../../../../core/ui/widgets/logo_widget.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final formKey = GlobalKey<FormState>();

  late TextEditingController username, password;

  AuthBloc authBloc = instance<AuthBloc>();

  AppPreferences appPreferences = instance<AppPreferences>();
  @override
  void initState() {
    username = TextEditingController();
    password = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    authBloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: SimpleAppBar(title: StringsManager.login.tr()),
      bottomNavigationBar: LoginFooterWidget(
        onTapRegister: () {
          //TODO: navigate to register page
        },
        onTapGuest: () {},
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            const LogoWidget(),
            Form(
              key: formKey,
              child: Column(children: [
                CustomTextField(
                  controller: username,
                  prefixIconPath: SvgManager.username,
                  textInputAction: TextInputAction.next,
                  hint: StringsManager.username.tr(),
                  validators: [RequiredValidator()],
                ),
                const SizedBox(height: 25),
                CustomTextField(
                  controller: password,
                  prefixIconPath: SvgManager.password,
                  isPassword: true,
                  hint: StringsManager.password.tr(),
                  validators: [RequiredValidator()],
                ),
              ]),
            ),
            Container(
              alignment: AlignmentDirectional.centerEnd,
              margin: const EdgeInsetsDirectional.only(end: 24),
              child: TextButton(
                  onPressed: () {
                    //TODO: navigate to forget password page
                  },
                  child: Text(StringsManager.forgetPassword.tr())),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 25),
              child: BlocConsumer(
                bloc: authBloc,
                listener: (_, state) {
                  if (state is LoginFailed) {
                    SnackBarManager.showSnackBar(
                        context: context, msg: state.msg);
                  }
                  if (state is LoginSuccessed) {
                    appPreferences.setUser(state.userModel).then((_) {
                      Navigator.of(context)
                          .pushReplacementNamed(RoutesManager.home);
                    });
                  }
                },
                builder: (context, state) {
                  return (state is LoginLoading)
                      ? const LoadingIndicator()
                      : FilledButton(
                          style: const ButtonStyle().copyWith(
                              fixedSize: MaterialStatePropertyAll(
                                  Size.fromWidth(ScreenManager.fromWidth(40)))),
                          onPressed: () {
                            if (formKey.currentState!.validate()) {
                              authBloc.add(LoginEvent(
                                  params: LoginParams(
                                      username: username.text,
                                      password: password.text)));
                            }
                          },
                          child: Text(StringsManager.login.tr()),
                        );
                },
              ),
            ),
            TextWithClickablePart(
                text: StringsManager.privacyPolicyTermsPrefix.tr(),
                clickableText: StringsManager.privacyPolicyTerms.tr(),
                onTap: () {
                  //TODO:Navigate to Privacy Policy Page
                }),
            const SizedBox(height: 20),
          ],
        ),
      ),
    );
  }
}
