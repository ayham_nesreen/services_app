import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../../../../core/ui/theme/border_radius_manager.dart';

class TextUnderIconButton extends StatelessWidget {
  final String iconPath;
  final String label;
  final Color color;
  final Function() onTap;
  const TextUnderIconButton({
    super.key,
    required this.iconPath,
    required this.label,
    required this.color,
    required this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      borderRadius: BorderRadiusManager.circular32,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(height: 20, child: SvgPicture.asset(iconPath, color: color)),
          const SizedBox(height: 4),
          Text(
            label,
            style: TextStyle(
                fontWeight: FontWeight.w500,
                color: color,
                decoration: TextDecoration.underline,
                decorationColor: color),
          )
        ],
      ),
    );
  }
}
