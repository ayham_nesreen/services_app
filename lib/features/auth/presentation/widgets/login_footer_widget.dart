import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

import '../../../../core/helpers/screen_manager.dart';
import '../../../../core/resources/strings_manager.dart';
import '../../../../core/resources/svg_manager.dart';
import '../../../../core/ui/theme/border_radius_manager.dart';
import '../../../../core/ui/theme/colors_manager.dart';
import 'text_with_icon_button.dart';

class LoginFooterWidget extends StatelessWidget {
  final Function() onTapRegister;
  final Function() onTapGuest;
  const LoginFooterWidget(
      {super.key, required this.onTapRegister, required this.onTapGuest});

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: AlignmentDirectional.bottomEnd,
      children: [
        Container(
          margin: const EdgeInsetsDirectional.only(end: 15),
          color: ColorsManager.secondary,
          width: ScreenManager.width,
          height: 50,
          padding: const EdgeInsets.symmetric(horizontal: 16),
          alignment: AlignmentDirectional.centerStart,
          child: Text(
            StringsManager.dontHaveAccount.tr(),
            style: const TextStyle(color: Colors.white),
          ),
        ),
        Container(
          margin: const EdgeInsetsDirectional.only(end: 15),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Container(
                width: 65,
                height: 75,
                margin: const EdgeInsets.only(bottom: 10),
                padding: const EdgeInsets.all(2),
                decoration: const BoxDecoration(
                    borderRadius: BorderRadiusManager.circular32,
                    color: ColorsManager.secondary),
                child: TextUnderIconButton(
                  color: Colors.white,
                  iconPath: SvgManager.arrowRegister,
                  label: StringsManager.register.tr(),
                  onTap: onTapRegister,
                ),
              ),
              Container(
                width: 65,
                height: 75,
                margin: const EdgeInsets.only(bottom: 10),
                padding: const EdgeInsets.all(2),
                decoration: const BoxDecoration(
                    borderRadius: BorderRadiusManager.circular32,
                    color: ColorsManager.backgroundColor),
                child: TextUnderIconButton(
                  color: ColorsManager.secondary,
                  iconPath: SvgManager.user,
                  label: StringsManager.guest.tr(),
                  onTap: onTapGuest,
                ),
              ),
            ],
          ),
        ),
        Container(
          width: 16,
          height: 60,
          decoration: const BoxDecoration(
              borderRadius: BorderRadiusManager.onlyTopStart25,
              color: ColorsManager.secondary),
        )
      ],
    );
  }
}
