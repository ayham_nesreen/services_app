import 'package:flutter/material.dart';

import '../../../../core/ui/theme/border_radius_manager.dart';
import '../../../../core/ui/theme/colors_manager.dart';
import '../../../../core/ui/theme/text_style_manager.dart';

class TextWithClickablePart extends StatelessWidget {
  final String text;
  final String clickableText;
  final Function() onTap;
  final EdgeInsetsGeometry margin;
  const TextWithClickablePart(
      {super.key,
      required this.text,
      required this.clickableText,
      required this.onTap,
      this.margin = const EdgeInsets.symmetric(horizontal: 36)});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: margin,
      child: RichText(
        textAlign: TextAlign.center,
        text: TextSpan(
          children: [
            WidgetSpan(
                child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 1),
              child: Text('$text ',
                  style: TextStyleManager.f11w400.copyWith(
                    color: ColorsManager.gray,
                  )),
            )),
            WidgetSpan(
                alignment: PlaceholderAlignment.middle,
                child: InkWell(
                    borderRadius: BorderRadiusManager.circular5,
                    overlayColor: MaterialStatePropertyAll(
                        ColorsManager.gray.withOpacity(0.15)),
                    onTap: onTap,
                    child: Padding(
                      padding: const EdgeInsets.all(4),
                      child: Text(clickableText,
                          style: TextStyleManager.f11w400.copyWith(
                            color: ColorsManager.gray,
                            decorationColor: ColorsManager.gray,
                            decoration: TextDecoration.underline,
                          )),
                    )))
          ],
        ),
      ),
    );
  }
}
