import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

import '../../../../core/helpers/screen_manager.dart';
import '../../../../core/navigation/routes/routes_manager.dart';
import '../../../../core/resources/strings_manager.dart';
import '../../../../core/ui/theme/colors_manager.dart';
import '../../../../core/ui/theme/text_style_manager.dart';
import '../widgets/custom_card.dart';
import '../widgets/expandable_widget.dart';
import '../widgets/label_text.dart';

class ServicesView extends StatelessWidget {
  const ServicesView({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 18),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: const EdgeInsetsDirectional.only(top: 18),
            alignment: AlignmentDirectional.centerEnd,
            child: FilledButton(
              style: const ButtonStyle()
                  .copyWith(elevation: const MaterialStatePropertyAll(2)),
              onPressed: () {},
              child: Text(StringsManager.showMyTasks.tr()),
            ),
          ),
          LabelText(text: StringsManager.services.tr()),
          Padding(
            padding: const EdgeInsetsDirectional.only(bottom: 15.0),
            child: Text(
              StringsManager.servicesDescription.tr(),
              style:
                  TextStyleManager.f13w400.copyWith(color: ColorsManager.gray),
            ),
          ),
          Expanded(
            child: ListView(
              children: const [
                ExpandableWidget(
                    title: 'Cardiology',
                    child: CustomCard(
                      padding: EdgeInsets.all(12),
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text('Transesophageal echocardiography'),
                            Text('Stress Testing'),
                          ]),
                    )),
                ExpandableWidget(
                    title: 'Addiction medicine Level 1',
                    child: ExpandableWidget(
                      title: 'Addiction medicine Levele2',
                      child: CustomCard(
                        padding: EdgeInsets.all(12),
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text('Transesophageal echocardiography'),
                              Text('Stress Testing'),
                            ]),
                      ),
                    )),
                ExpandableWidget(title: 'Adolescent medicine'),
                ExpandableWidget(title: 'Aerospace medicine'),
                ExpandableWidget(title: 'Anaesthesia'),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 15.0),
            child: FilledButton(
                onPressed: () {
                  Navigator.of(context).pushNamed(RoutesManager.addService);
                },
                style: const ButtonStyle().copyWith(
                    fixedSize: MaterialStatePropertyAll(
                        Size.fromWidth(ScreenManager.width))),
                child: Text(StringsManager.linkNewService.tr())),
          )
        ],
      ),
    );
  }
}
