import 'package:flutter/material.dart';

import '../../../../core/helpers/screen_manager.dart';
import '../../../../core/ui/theme/border_radius_manager.dart';
import '../../../../core/ui/theme/box_decoration_manager.dart';

class CustomCard extends StatelessWidget {
  final Widget child;
  final double? height;
  final EdgeInsetsGeometry? padding;
  final Function()? onTap;

  const CustomCard({
    super.key,
    required this.child,
    this.height,
    this.padding,
    this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    return InkWell(
      borderRadius: BorderRadiusManager.circular15,
      onTap: onTap,
      child: Container(
          height: height,
          width: ScreenManager.width,
          padding: padding,
          decoration: BoxDecorationManager.whiteBox,
          child: child),
    );
  }
}
