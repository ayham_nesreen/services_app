import 'package:flutter/material.dart';

import '../../../../core/ui/theme/button_style_manager.dart';
import '../../../../core/ui/theme/text_style_manager.dart';
import 'custom_card.dart';
import 'ribbon_widget.dart';

class ExpandableWidget extends StatefulWidget {
  final String title;
  final Widget? child;
  final double height;
  const ExpandableWidget({
    super.key,
    required this.title,
    this.child,
    this.height = 50,
  });

  @override
  State<ExpandableWidget> createState() => _ExpandableWidgetState();
}

class _ExpandableWidgetState extends State<ExpandableWidget> {
  bool toggle = false;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsetsDirectional.only(bottom: 5),
      child: Column(
        children: [
          CustomCard(
            onTap: widget.child == null
                ? null
                : () => setState(() => toggle = !toggle),
            height: widget.height,
            padding: const EdgeInsetsDirectional.only(end: 12),
            child: Row(children: [
              if (toggle) RibbonWidget(height: widget.height),
              SizedBox(width: toggle ? 12 : 20),
              Expanded(
                  child: Text(widget.title, style: TextStyleManager.f16w500)),
              if (widget.child != null)
                IconButton(
                  style: ButtonStyleManager.filledIconButton,
                  icon: Center(
                    child: Icon(
                        toggle
                            ? Icons.keyboard_arrow_up_rounded
                            : Icons.keyboard_arrow_down_rounded,
                        color: Colors.white),
                  ),
                  onPressed: () => setState(() => toggle = !toggle),
                )
            ]),
          ),
          const SizedBox(height: 8),
          AnimatedSwitcher(
            duration: const Duration(milliseconds: 150),
            child: !toggle || widget.child == null
                ? const SizedBox.shrink()
                : Padding(
                    padding: const EdgeInsets.only(bottom: 8),
                    child: widget.child!,
                  ),
            transitionBuilder: (child, animation) =>
                SizeTransition(sizeFactor: animation, child: child),
          )
        ],
      ),
    );
  }
}
