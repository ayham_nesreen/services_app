import 'package:flutter/material.dart';
import 'package:services_app/core/ui/theme/text_style_manager.dart';

import '../../../../core/ui/theme/border_radius_manager.dart';
import '../../../../core/ui/theme/colors_manager.dart';
import '../../../../core/ui/theme/shadows_manager.dart';

class CustomCheckboxListTile extends StatefulWidget {
  final bool checkboxVisiblity;
  final String label;
  final bool initValue;
  final Function(bool value) onChanged;
  final EdgeInsetsGeometry padding;
  const CustomCheckboxListTile(
      {super.key,
      this.checkboxVisiblity = true,
      required this.label,
      required this.initValue,
      required this.onChanged,
      this.padding = const EdgeInsets.symmetric(horizontal: 24, vertical: 6)});

  @override
  State<CustomCheckboxListTile> createState() => _CustomCheckboxListTileState();
}

class _CustomCheckboxListTileState extends State<CustomCheckboxListTile> {
  late ValueNotifier<bool> isSelected;

  @override
  void initState() {
    isSelected = ValueNotifier<bool>(widget.initValue);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      overlayColor: const MaterialStatePropertyAll(Colors.transparent),
      onTap: () {
        if (widget.checkboxVisiblity) {
          isSelected.value = !isSelected.value;
          widget.onChanged(isSelected.value);
        }
      },
      child: Container(
        padding: widget.padding,
        child: Row(crossAxisAlignment: CrossAxisAlignment.center, children: [
          (widget.checkboxVisiblity)
              ? ValueListenableBuilder(
                  valueListenable: isSelected,
                  builder: (_, __, ___) {
                    return Stack(
                      alignment: Alignment.center,
                      children: [
                        Container(
                          height: 23,
                          width: 23,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadiusManager.circular5,
                              border: Border.all(
                                  width: 2,
                                  color: isSelected.value
                                      ? ColorsManager.primary
                                      : ColorsManager.primary.withOpacity(0.5)),
                              boxShadow: [ShadowsManager.checkboxShadow]),
                        ),
                        AnimatedSwitcher(
                          duration: const Duration(milliseconds: 150),
                          transitionBuilder: (child, animation) =>
                              ScaleTransition(scale: animation, child: child),
                          child: isSelected.value
                              ? Container(
                                  height: 12,
                                  width: 12,
                                  decoration: const BoxDecoration(
                                      color: ColorsManager.primary,
                                      borderRadius:
                                          BorderRadiusManager.circularFull),
                                )
                              : const SizedBox.shrink(),
                        )
                      ],
                    );
                  })
              : const SizedBox(width: 23),
          const SizedBox(width: 15),
          Expanded(
              child: Text(
            widget.label,
            style: TextStyleManager.f16w400,
          ))
        ]),
      ),
    );
  }
}
