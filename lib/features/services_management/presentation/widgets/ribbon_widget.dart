import 'package:flutter/material.dart';

import '../../../../core/ui/theme/border_radius_manager.dart';
import '../../../../core/ui/theme/colors_manager.dart';

class RibbonWidget extends StatelessWidget {
  final double height;
  final double width;
  final Color color;
  const RibbonWidget({
    super.key,
    required this.height,
    this.width = 8,
    this.color = ColorsManager.primary,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: color, borderRadius: BorderRadiusManager.onlyStart15),
      width: width,
      height: height,
    );
  }
}
