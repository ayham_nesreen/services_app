import 'package:flutter/material.dart';

import '../../../../core/ui/theme/border_radius_manager.dart';
import '../../../../core/ui/theme/colors_manager.dart';
import '../../../../core/ui/theme/text_style_manager.dart';

class LabelText extends StatelessWidget {
  final String text;
  final TextStyle style;
  final EdgeInsetsGeometry padding;
  const LabelText(
      {super.key,
      required this.text,
      this.style = TextStyleManager.f16w500,
      this.padding = const EdgeInsets.only(bottom: 15)});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: padding,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            text,
            style: style,
          ),
          Container(
            padding: const EdgeInsets.only(top: 2),
            height: 6,
            width: (text.length * 10) / 2,
            decoration: const BoxDecoration(
                borderRadius: BorderRadiusManager.circular18,
                color: ColorsManager.primary),
          )
        ],
      ),
    );
  }
}
