import 'package:flutter/material.dart';

import '../../../../core/ui/theme/colors_manager.dart';
import '../../data/models/tree_model.dart';
import 'custom_checkbox_list_tile.dart';

class CheckboxTileTree extends StatefulWidget {
  final TreeModel treeModel;
  final bool initValue;
  final Function(List<int> ids) onChanged;
  final List<int> selectedIds;
  const CheckboxTileTree({
    super.key,
    required this.treeModel,
    required this.initValue,
    required this.onChanged,
    required this.selectedIds,
  });

  @override
  State<CheckboxTileTree> createState() => _CheckboxTileTreeState();
}

class _CheckboxTileTreeState extends State<CheckboxTileTree> {
  bool toggle = false;
  Set<int> selectedItemsIds = {};
  @override
  void initState() {
    selectedItemsIds.addAll(widget.selectedIds);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 4),
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.symmetric(
                horizontal: 12,
                vertical: (widget.treeModel.items.isEmpty) ? 22 : 8),
            color: Colors.white,
            child: Row(children: [
              Expanded(
                child: CustomCheckboxListTile(
                  checkboxVisiblity: widget.treeModel.items.isEmpty,
                  label: widget.treeModel.name,
                  initValue: widget.initValue,
                  onChanged: (value) {
                    if (value) {
                      selectedItemsIds.add(widget.treeModel.id);
                    } else {
                      selectedItemsIds.remove(widget.treeModel.id);
                    }
                    widget.onChanged(selectedItemsIds.toList());
                  },
                  padding: const EdgeInsetsDirectional.only(start: 12),
                ),
              ),
              if (widget.treeModel.items.isNotEmpty)
                IconButton(
                  icon: Icon(
                      toggle
                          ? Icons.keyboard_arrow_up_rounded
                          : Icons.keyboard_arrow_down_rounded,
                      color: ColorsManager.primary),
                  onPressed: () => setState(() => toggle = !toggle),
                )
            ]),
          ),
          const SizedBox(height: 8),
          AnimatedSwitcher(
            duration: const Duration(milliseconds: 150),
            child: !toggle || (widget.treeModel.items.isEmpty)
                ? const SizedBox.shrink()
                : (widget.treeModel.items.isNotEmpty)
                    ? Column(
                        children: [
                          ...widget.treeModel.items.map(
                            (e) {
                              return e.items.isEmpty
                                  ? CustomCheckboxListTile(
                                      label: e.name,
                                      initValue:
                                          selectedItemsIds.contains(e.id),
                                      onChanged: (value) {
                                        if (value) {
                                          selectedItemsIds.add(e.id);
                                          selectedItemsIds
                                              .add(widget.treeModel.id);
                                        } else {
                                          selectedItemsIds.remove(e.id);
                                          selectedItemsIds
                                              .remove(widget.treeModel.id);
                                        }
                                        widget.onChanged(
                                            selectedItemsIds.toList());
                                      },
                                    )
                                  : CheckboxTileTree(
                                      selectedIds: selectedItemsIds.toList(),
                                      initValue:
                                          selectedItemsIds.contains(e.id),
                                      onChanged: (values) {
                                        selectedItemsIds = values.toSet();
                                        widget.onChanged(
                                            selectedItemsIds.toList());
                                      },
                                      treeModel: e,
                                    );
                            },
                          ).toList()
                        ],
                      )
                    : const SizedBox.shrink(),
            transitionBuilder: (child, animation) =>
                SizeTransition(sizeFactor: animation, child: child),
          )
        ],
      ),
    );
  }
}
