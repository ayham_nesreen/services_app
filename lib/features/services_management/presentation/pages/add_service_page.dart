import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:services_app/core/helpers/screen_manager.dart';
import 'package:services_app/core/ui/widgets/searchable_app_bar.dart';
import 'package:services_app/features/services_management/data/models/tree_model.dart';

import '../../../../core/resources/strings_manager.dart';
import '../widgets/checkbox_tile_tree.dart';

class AddServicePage extends StatefulWidget {
  const AddServicePage({super.key});

  @override
  State<AddServicePage> createState() => _AddServicePageState();
}

class _AddServicePageState extends State<AddServicePage> {
  List<TreeModel> trees = [
    TreeModel(id: 1, name: 'Level 1', items: [
      TreeModel(id: 2, name: 'Level 2', items: [
        TreeModel(id: 3, name: 'Level 3-1', items: []),
        TreeModel(id: 4, name: 'Level 3-2', items: []),
      ])
    ]),
    TreeModel(id: 5, name: 'Cardiology', items: [
      TreeModel(id: 6, name: 'Stress Testing', items: []),
      TreeModel(id: 7, name: 'Holter Monitoring', items: []),
      TreeModel(id: 8, name: 'Echo heart color doppler', items: []),
    ]),
    TreeModel(id: 9, name: 'Addication medicine', items: []),
    TreeModel(id: 10, name: 'Adolescent medicine', items: []),
    TreeModel(id: 11, name: 'Aerospace medicine', items: []),
  ];

  List<int> selectedIds = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: FilledButton(
          style: const ButtonStyle().copyWith(
              fixedSize: MaterialStatePropertyAll(
                  Size.fromWidth(ScreenManager.fromWidth(90)))),
          child: Text(StringsManager.done.tr()),
          onPressed: () {}),
      body: Column(children: [
        SearchableAppBar(
          title: StringsManager.addNewServices.tr(),
          onSearch: (query) {},
        ),
        Expanded(
            child: ListView.builder(
          padding: const EdgeInsets.only(bottom: 80),
          itemCount: trees.length,
          itemBuilder: (_, index) {
            return CheckboxTileTree(
              treeModel: trees[index],
              initValue: false,
              selectedIds: selectedIds,
              onChanged: (values) {
                selectedIds = values;
              },
            );
          },
        ))
      ]),
    );
  }
}
