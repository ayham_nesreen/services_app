import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

import '../../../../core/resources/strings_manager.dart';
import '../../../../core/ui/widgets/base_app_bar.dart';
import '../views/services_view.dart';

class ServicesPage extends StatefulWidget {
  const ServicesPage({super.key});

  @override
  State<ServicesPage> createState() => _ServicesPageState();
}

class _ServicesPageState extends State<ServicesPage> {
  List<Tab> tabs = const [
    Tab(icon: Icon(Icons.person_2_rounded)),
    Tab(icon: Icon(Icons.apartment)),
    Tab(icon: Icon(Icons.clean_hands_rounded)),
  ];

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: tabs.length,
      child: Scaffold(
        appBar: BaseAppBar(
          title: StringsManager.dummyClinicName.tr(),
          bottom: TabBar(tabs: tabs),
          actions: [
            IconButton(
                onPressed: () {},
                icon: const Icon(
                  Icons.message_rounded,
                  color: Colors.white,
                ))
          ],
        ),
        body: const TabBarView(children: [
          Center(child: Text('Tab 1')),
          Center(child: Text('Tab 2')),
          ServicesView(),
        ]),
      ),
    );
  }
}
