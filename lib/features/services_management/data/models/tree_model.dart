class TreeModel {
  int id;
  String name;
  List<TreeModel> items;

  TreeModel({required this.id, required this.name, required this.items});
}
