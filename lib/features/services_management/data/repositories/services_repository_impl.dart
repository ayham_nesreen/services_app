import '../../../../core/structure_utils/network/network_info.dart';
import '../../domain/repositories/services_repository.dart';

import '../datasources/services_data_source.dart';

class ServicesRepositoryImpl implements ServicesRepository {
  final ServicesDataSource servicesDataSource;

  final NetworkInfo networkInfo;

  ServicesRepositoryImpl({
    required this.servicesDataSource,
    required this.networkInfo,
  });
}
