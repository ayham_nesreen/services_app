import 'services_data_source.dart';

import '../../../../core/api/request_manager.dart';

class ServicesDataSourceImpl extends ServicesDataSource {
  final RequestManager requestManager;

  ServicesDataSourceImpl({required this.requestManager});
}
