import 'package:services_app/core/models/paginate_model.dart';
import 'package:services_app/features/notifications/data/models/product.dart';
import 'package:services_app/features/notifications/domain/params/get_notifications_params.dart';

abstract class NotificationsDataSource {
  Future<PaginateModel<Product>> getNotifications(
      GetNotificationsParams params);
}
