import 'package:services_app/core/api/urls_manager.dart';
import 'package:services_app/core/enums/http_method.dart';
import 'package:services_app/core/models/paginate_model.dart';

import 'package:services_app/features/notifications/data/models/product.dart';

import 'package:services_app/features/notifications/domain/params/get_notifications_params.dart';

import 'notifications_data_source.dart';

import '../../../../core/api/request_manager.dart';

class NotificationsDataSourceImpl extends NotificationsDataSource {
  final RequestManager requestManager;

  NotificationsDataSourceImpl({required this.requestManager});

  @override
  Future<PaginateModel<Product>> getNotifications(
      GetNotificationsParams params) async {
    final response = await requestManager.sendRequest(
        method: HttpMethod.GET,
        url: UrlsManager.getNotifications,
        withAuthentication: true,
        data: params.toJson());

    return PaginateModel.fromJson(
        response, response['products'], (json) => Product.fromJson(json));
  }
}
