import 'package:dartz/dartz.dart';
import 'package:services_app/core/helpers/helper_functions.dart';

import 'package:services_app/core/models/paginate_model.dart';

import 'package:services_app/core/structure_utils/errors/failures.dart';

import 'package:services_app/features/notifications/data/models/product.dart';

import 'package:services_app/features/notifications/domain/params/get_notifications_params.dart';

import '../../../../core/structure_utils/network/network_info.dart';
import '../../domain/repositories/notifications_repository.dart';

import '../datasources/notifications_data_source.dart';

class NotificationsRepositoryImpl implements NotificationsRepository {
  final NotificationsDataSource notificationsDataSource;

  final NetworkInfo networkInfo;

  NotificationsRepositoryImpl({
    required this.notificationsDataSource,
    required this.networkInfo,
  });

  @override
  Future<Either<Failure, PaginateModel<Product>>> getNotifications(
      GetNotificationsParams params) {
    return HelperFunctions.tryToExecute<PaginateModel<Product>>(
        networkInfo: networkInfo,
        execute: () => notificationsDataSource.getNotifications(params));
  }
}
