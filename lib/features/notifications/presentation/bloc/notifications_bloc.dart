import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:services_app/core/extentions/failure_mapper.dart';
import 'package:services_app/core/models/paginate_model.dart';
import 'package:services_app/features/notifications/data/models/product.dart';
import 'package:services_app/features/notifications/domain/params/get_notifications_params.dart';
import 'package:services_app/features/notifications/domain/usecases/get_notifications_use_case.dart';

part 'notifications_event.dart';
part 'notifications_state.dart';

class NotificationsBloc extends Bloc<NotificationsEvent, NotificationsState> {
  PaginateModel<Product> products = PaginateModel<Product>(items: []);

  GetNotificationsUseCase getNotificationsUseCase;
  NotificationsBloc({required this.getNotificationsUseCase})
      : super(NotificationsInitial()) {
    on<NotificationsEvent>((event, emit) async {
      if (event is GetNotifications) {
        emit(Loading());
        (await getNotificationsUseCase.call(event.params)).fold(
            (l) => emit(GetNotificationsFailed(msg: l.toErrorMessage())), (r) {
          products.items.addAll(r.items);
          products.total = r.total;
          products.skip = r.skip;
          products.limit = r.limit;
          emit(GetNotificationsSuccessed());
        });
      }
    });
  }
}
