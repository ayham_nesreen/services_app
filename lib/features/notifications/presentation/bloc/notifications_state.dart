part of 'notifications_bloc.dart';

class NotificationsState {}

class NotificationsInitial extends NotificationsState {}

class Loading extends NotificationsState {}

class GetNotificationsFailed extends NotificationsState {
  final String msg;
  GetNotificationsFailed({required this.msg});
}

class GetNotificationsSuccessed extends NotificationsState {}
