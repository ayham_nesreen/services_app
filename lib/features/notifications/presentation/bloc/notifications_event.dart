part of 'notifications_bloc.dart';

class NotificationsEvent {}

class GetNotifications extends NotificationsEvent {
  final GetNotificationsParams params;
  GetNotifications({required this.params});
}
