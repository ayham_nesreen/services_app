import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:services_app/core/ui/overlay_widget_manager/snack_bar_manager.dart';
import 'package:services_app/core/ui/widgets/paginate_list.dart';
import 'package:services_app/features/notifications/domain/params/get_notifications_params.dart';
import 'package:services_app/features/notifications/presentation/bloc/notifications_bloc.dart';

import '../../../../core/resources/strings_manager.dart';
import '../../../../core/ui/widgets/base_app_bar.dart';
import '../../../../di/injection_container.dart';
import '../../data/models/product.dart';
import '../widgets/notification_widget.dart';
import '../widgets/notification_widget_skeleton.dart';

class NotificationsPage extends StatefulWidget {
  const NotificationsPage({super.key});

  @override
  State<NotificationsPage> createState() => _NotificationsPageState();
}

class _NotificationsPageState extends State<NotificationsPage> {
  NotificationsBloc notificationsBloc = instance<NotificationsBloc>();
  GetNotificationsParams params = GetNotificationsParams(skip: 0, limit: 10);

  bool isLoading = true;
  @override
  void initState() {
    notificationsBloc.add(GetNotifications(params: params));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BaseAppBar(title: StringsManager.notifications.tr()),
      body: BlocConsumer(
        bloc: notificationsBloc,
        listener: (_, state) {
          if (state is GetNotificationsFailed) {
            isLoading = false;
            SnackBarManager.showSnackBar(context: context, msg: state.msg);
          }
          if (state is GetNotificationsSuccessed) {
            isLoading = false;
            params.skip += params.limit;
          }
        },
        builder: (_, state) {
          return PaginateList<Product>(
              skip: params.skip,
              total: notificationsBloc.products.total,
              isLoading: isLoading,
              items: notificationsBloc.products.items,
              call: () {
                isLoading = true;
                notificationsBloc.add(GetNotifications(params: params));
              },
              skeletonWidget: const NotificationWidgetSkeleton(),
              itemBuilder: (product) => NotificationWidget(product: product));
        },
      ),
    );
  }
}
