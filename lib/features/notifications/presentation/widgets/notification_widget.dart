import 'package:flutter/material.dart';
import 'package:services_app/core/ui/widgets/custom_network_image.dart';
import 'package:services_app/features/notifications/data/models/product.dart';

import '../../../../core/ui/theme/text_style_manager.dart';

class NotificationWidget extends StatelessWidget {
  final Product product;
  const NotificationWidget({super.key, required this.product});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16),
      child: Row(children: [
        CustomNetworkImage(imageUrl: product.thumbnail ?? ''),
        const SizedBox(width: 20),
        Expanded(
            child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(product.title ?? '',
                style: TextStyleManager.f14w600.copyWith(color: Colors.black)),
            Text(product.description ?? '',
                style:
                    TextStyleManager.f14w400.copyWith(color: Colors.black87)),
          ],
        ))
      ]),
    );
  }
}
