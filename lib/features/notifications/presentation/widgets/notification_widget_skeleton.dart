import 'package:flutter/material.dart';
import 'package:services_app/core/helpers/screen_manager.dart';
import 'package:services_app/core/ui/theme/border_radius_manager.dart';
import 'package:services_app/core/ui/widgets/skeleton_widget.dart';
import 'package:services_app/core/ui/widgets/text_skeleton.dart';

import '../../../../core/ui/theme/colors_manager.dart';

class NotificationWidgetSkeleton extends StatelessWidget {
  const NotificationWidgetSkeleton({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16),
      child: Row(children: [
        SkeletonWidget(
            height: ScreenManager.fromWidth(20),
            width: ScreenManager.fromWidth(20),
            decoration: BoxDecoration(
                color: ColorsManager.gray30,
                borderRadius: BorderRadiusManager.circularFull)),
        const SizedBox(width: 20),
        Expanded(
            child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            TextSkeleton(width: ScreenManager.fromWidth(35)),
            const SizedBox(height: 10),
            TextSkeleton(width: ScreenManager.fromWidth(60)),
          ],
        ))
      ]),
    );
  }
}
