import 'package:dartz/dartz.dart';
import 'package:services_app/core/models/paginate_model.dart';
import 'package:services_app/core/structure_utils/errors/failures.dart';
import 'package:services_app/core/structure_utils/usecases/usecase.dart';
import 'package:services_app/features/notifications/data/models/product.dart';
import 'package:services_app/features/notifications/domain/params/get_notifications_params.dart';
import 'package:services_app/features/notifications/domain/repositories/notifications_repository.dart';

class GetNotificationsUseCase
    implements UseCase<PaginateModel<Product>, GetNotificationsParams> {
  final NotificationsRepository repository;
  GetNotificationsUseCase(this.repository);
  @override
  Future<Either<Failure, PaginateModel<Product>>> call(
      GetNotificationsParams params) {
    return repository.getNotifications(params);
  }
}
