import 'package:dartz/dartz.dart';
import 'package:services_app/core/models/paginate_model.dart';
import 'package:services_app/core/structure_utils/errors/failures.dart';
import 'package:services_app/features/notifications/data/models/product.dart';
import 'package:services_app/features/notifications/domain/params/get_notifications_params.dart';

abstract class NotificationsRepository {
  Future<Either<Failure, PaginateModel<Product>>> getNotifications(
      GetNotificationsParams params);
}
