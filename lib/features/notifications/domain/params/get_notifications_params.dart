class GetNotificationsParams {
  int limit;
  int skip;

  GetNotificationsParams({this.limit = 10, required this.skip});

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data["limit"] = limit;
    data["skip"] = skip;
    return data;
  }
}
