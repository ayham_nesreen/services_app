import 'package:services_app/features/auth/data/repositories/auth_repository_impl.dart';
import 'package:services_app/features/auth/domain/repositories/auth_repository.dart';
import 'package:services_app/features/auth/domain/usecases/login_use_case.dart';

import '../features/auth/data/datasources/auth_data_source.dart';
import '../features/auth/data/datasources/auth_data_source_impl.dart';
import '../features/auth/presentation/bloc/auth_bloc.dart';
import 'injection_container.dart';

void initAuth() async {
  if (!instance.isRegistered<AuthBloc>()) {
    //Bloc
    instance
        .registerFactory<AuthBloc>(() => AuthBloc(loginUseCase: instance()));

    //Usecase
    instance
        .registerLazySingleton<LoginUseCase>(() => LoginUseCase(instance()));

    //Repository
    instance.registerLazySingleton<AuthRepository>(() => AuthRepositoryImpl(
        authDataSource: instance(), networkInfo: instance()));

    //Datasource
    instance.registerLazySingleton<AuthDataSource>(
        () => AuthDataSourceImpl(requestManager: instance()));
  }
}
