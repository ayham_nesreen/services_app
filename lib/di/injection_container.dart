import 'dart:developer';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:dio/dio.dart';
import 'package:get_it/get_it.dart';

import 'package:pretty_dio_logger/pretty_dio_logger.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../core/api/request_manager.dart';
import '../core/preferences/app_preferences.dart';
import '../core/structure_utils/network/network_info.dart';
import '../core/structure_utils/network/network_info_impl.dart';

final instance = GetIt.instance;

Future<void> initCore() async {
  //local data source

  final sharedPrefs = await SharedPreferences.getInstance();
  instance.registerLazySingleton<SharedPreferences>(() => sharedPrefs);

  instance.registerLazySingleton<AppPreferences>(
      () => AppPreferences(preferences: instance()));

  //Dio
  instance.registerLazySingleton<Dio>(() => Dio());

  //Logger
  instance.registerLazySingleton<PrettyDioLogger>(() => PrettyDioLogger(
        responseBody: true,
        error: true,
        requestBody: true,
        requestHeader: true,
        maxWidth: 118,
        logPrint: (object) {
          log('$object');
        },
      ));

  //RequestManager
  instance.registerLazySingleton<RequestManager>(() => RequestManager(
      dio: instance(),
      prettyDioLogger: instance(),
      appPreferences: instance()));

  //Connectivity
  instance.registerLazySingleton<Connectivity>(() => Connectivity());

  //NetworkInfo
  instance.registerLazySingleton<NetworkInfo>(
      () => NetworkInfoImpl(connectivity: instance()));
}
