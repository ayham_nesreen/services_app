import '../features/notifications/data/datasources/notifications_data_source.dart';
import '../features/notifications/data/datasources/notifications_data_source_impl.dart';
import '../features/notifications/data/repositories/notifications_repository_impl.dart';
import '../features/notifications/domain/repositories/notifications_repository.dart';
import '../features/notifications/domain/usecases/get_notifications_use_case.dart';
import '../features/notifications/presentation/bloc/notifications_bloc.dart';
import 'injection_container.dart';

void initNotifications() async {
  if (!instance.isRegistered<NotificationsBloc>()) {
    //Bloc
    instance.registerFactory<NotificationsBloc>(
        () => NotificationsBloc(getNotificationsUseCase: instance()));

    //Usecase
    instance.registerLazySingleton<GetNotificationsUseCase>(
        () => GetNotificationsUseCase(instance()));

    //Repository
    instance.registerLazySingleton<NotificationsRepository>(() =>
        NotificationsRepositoryImpl(
            notificationsDataSource: instance(), networkInfo: instance()));

    //Datasource
    instance.registerLazySingleton<NotificationsDataSource>(
        () => NotificationsDataSourceImpl(requestManager: instance()));
  }
}
