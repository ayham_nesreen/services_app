import '../resources/error_messages_manager.dart';
import 'base_validator.dart';
import 'package:easy_localization/easy_localization.dart';

class RequiredValidator extends BaseValidator {
  @override
  bool checkIfRequired() {
    return true;
  }

  @override
  String getValidateMessage() {
    return ErrorMsgManager.fieldRequired.tr();
  }

  @override
  bool validateFunction(String value) {
    return value.isNotEmpty;
  }
}
