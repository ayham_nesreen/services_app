import 'package:easy_localization/easy_localization.dart';

import '../resources/error_messages_manager.dart';

abstract class BaseValidator {
  bool checkIfRequired();

  bool validateFunction(String value);

  String getValidateMessage();

  static String? validateValue(
    String value,
    List<BaseValidator> validators,
    bool isValidationActive,
  ) {
    if (!isValidationActive) return null;
    for (int i = 0; i < validators.length; i++) {
      if (!validators[i].validateFunction(value)) {
        return (value.isNotEmpty)
            ? validators[i].getValidateMessage()
            : validators[i].checkIfRequired()
                ? ErrorMsgManager.fieldRequired.tr()
                : null;
      }
    }
    return null;
  }
}
