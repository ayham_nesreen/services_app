abstract class Failure {
  final String? error;
  Failure({this.error});
}

class DataFailure extends Failure {
  DataFailure({required String error}) : super(error: error);
}

class ConnectionFailure extends Failure {}

class TimeoutFailure extends Failure {}

class BadResponseFailure extends Failure {}

class RequestCancelledFailure extends Failure {}

class BadCertificateFailure extends Failure {}

class UnknownFailure extends Failure {}
