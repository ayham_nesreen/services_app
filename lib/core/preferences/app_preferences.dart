import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

import '../../features/auth/data/models/user_model.dart';
import 'preferences_keys.dart';

class AppPreferences {
  final SharedPreferences preferences;
  AppPreferences({required this.preferences});

  Future<bool> clearData() async {
    return preferences.clear();
  }

  Future<bool> removeKey(String key) {
    return preferences.remove(key);
  }

  Future<bool> setString({required String key, required String value}) {
    return preferences.setString(key, value);
  }

  String? getString(String key) {
    return preferences.getString(key);
  }

  Future<bool> setInt({required String key, required int value}) {
    return preferences.setInt(key, value);
  }

  int? getInt(String key) {
    return preferences.getInt(key);
  }

  Future<bool> setUser(UserModel userModel) async {
    bool user = false, token = false;
    user = await preferences.setString(
        PreferenceKeys.user, json.encode(userModel));
    token = await saveUserToken(userModel.token ?? '');
    return (user && token);
  }

  UserModel? getUser() {
    final data = preferences.getString(PreferenceKeys.user);
    return data == null ? null : UserModel.fromJson(json.decode(data));
  }

  Future<bool> saveUserToken(String token) async {
    return await preferences.setString(PreferenceKeys.token, token);
  }

  String getToken() {
    return preferences.getString(PreferenceKeys.token) ?? '';
  }
}
