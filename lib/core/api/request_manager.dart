import 'dart:convert';
import 'dart:developer';

import 'package:dio/dio.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';
import 'package:services_app/core/preferences/app_preferences.dart';
import 'package:services_app/core/resources/error_messages_manager.dart';

import '../enums/http_method.dart';
import '../structure_utils/errors/exceptions.dart';

class RequestManager {
  final Dio dio;
  final AppPreferences appPreferences;
  final PrettyDioLogger prettyDioLogger;

  RequestManager({
    required this.dio,
    required this.appPreferences,
    required this.prettyDioLogger,
  });
  Future sendRequest({
    required HttpMethod method,
    required String url,
    bool withAuthentication = false,
    var data,
  }) async {
    late Response response;

    if (!dio.interceptors.contains(prettyDioLogger)) {
      dio.interceptors.add(prettyDioLogger);
    }

    final Map<String, String> headers = {
      'Content-Type': 'application/json',
      "Accept": 'application/json',
    };
    log('$headers', name: 'header');
    if (withAuthentication) {
      final String token = appPreferences.getToken();
      headers.putIfAbsent("Authorization", () => 'Bearer $token');
    }

    Options options = Options(
        headers: headers,
        followRedirects: false,
        receiveTimeout: const Duration(seconds: 15),
        sendTimeout: const Duration(seconds: 15),
        validateStatus: (status) {
          return status! < 501;
        });

    try {
      switch (method) {
        case HttpMethod.GET:
          response =
              await dio.get(url, queryParameters: data, options: options);
          break;

        case HttpMethod.POST:
          response = await dio.post(url, data: data, options: options);
          break;

        case HttpMethod.PUT:
          response = await dio.put(url,
              data: data, queryParameters: data, options: options);
          break;

        case HttpMethod.DELETE:
          response = await dio.delete(url,
              data: data, queryParameters: data, options: options);
          break;
      }

      if (response.statusCode != 200) {
        if (response.data["message"] != null) {
          throw DataException(error: response.data["message"]);
        } else {
          throw DataException(error: ErrorMsgManager.unexcpectedError);
        }
      } else if (response.statusCode == 500) {
        throw BadResponseException();
      }
      dynamic decoded;

      decoded =
          response.data is String ? json.decode(response.data) : response.data;
      return decoded;
    } on DioException catch (e) {
      switch (e.type) {
        case DioExceptionType.connectionError:
          throw ConnectionException();

        case DioExceptionType.sendTimeout:
          throw TimeoutException();

        case DioExceptionType.receiveTimeout:
          throw TimeoutException();

        case DioExceptionType.connectionTimeout:
          throw TimeoutException();

        case DioExceptionType.badResponse:
          throw BadResponseException();

        case DioExceptionType.cancel:
          throw RequestCancelledException();

        case DioExceptionType.badCertificate:
          throw BadCertificateException();

        case DioExceptionType.unknown:
          log('${e.error}');
          throw UnknownException();
      }
    }
  }
}
