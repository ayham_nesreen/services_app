import 'package:easy_localization/easy_localization.dart';

import '../structure_utils/errors/failures.dart';
import '../resources/error_messages_manager.dart';

extension FailureMapper on Failure {
  ///map any Faliure to error message
  String toErrorMessage() {
    if (this is DataFailure) {
      return error!;
    } else if (this is ConnectionFailure) {
      return ErrorMsgManager.connectionError.tr();
    } else if (this is TimeoutFailure) {
      return ErrorMsgManager.timeoutError.tr();
    } else if (this is BadResponseFailure) {
      return ErrorMsgManager.badResponse.tr();
    } else if (this is RequestCancelledFailure) {
      return ErrorMsgManager.requestCancelled.tr();
    } else if (this is BadCertificateFailure) {
      return ErrorMsgManager.badCertificate.tr();
    } else if (this is UnknownFailure) {
      return ErrorMsgManager.unknownError.tr();
    }
    return ErrorMsgManager.unexcpectedError.tr();
  }
}
