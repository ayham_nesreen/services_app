extension StringInjector on String {
  ///Usage
  ///-----
  ///inject Url-Parameter in url.
  ///____________________________
  ///Params
  ///------
  ///- `value` refer to the value of the parameter.
  ///- `parameterName` refer to the name of parameter `( regardless of : )`.
  String injectUrlParameter({required parameterName, required var value}) {
    return replaceAll(':$parameterName', '$value');
  }
}
