extension ListChecker on List? {
  ///return `true` if list is `empty` or `null`, otherwise return `false`
  bool isNullOrEmpty() {
    if (this == null) {
      return true;
    } else if (this!.isEmpty) {
      return true;
    }
    return false;
  }
}
