import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

class TransitionsBuilderManager {
  static slideHorizontal(context, animation, secondaryAnimation, child) {
    var begin = EasyLocalization.of(context)!.locale.languageCode == 'en'
        ? const Offset(1.0, 0.0)
        : const Offset(-1.0, 0.0);
    const end = Offset.zero;
    const curve = Curves.ease;

    var tween = Tween(begin: begin, end: end).chain(CurveTween(curve: curve));

    return SlideTransition(
      position: animation.drive(tween),
      child: child,
    );
  }

  static slideVertical(context, animation, secondaryAnimation, child) {
    const begin = Offset(0.0, 1.0);
    const end = Offset.zero;
    const curve = Curves.ease;

    var tween = Tween(begin: begin, end: end).chain(CurveTween(curve: curve));

    return SlideTransition(
      position: animation.drive(tween),
      child: child,
    );
  }

  static fade(context, animation, secondaryAnimation, child) {
    animation = CurvedAnimation(curve: Curves.easeInOut, parent: animation);
    return FadeTransition(
      opacity: animation,
      child: child,
    );
  }
}
