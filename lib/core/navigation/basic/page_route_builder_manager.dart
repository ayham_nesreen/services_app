import 'package:flutter/material.dart';

import '../../enums/navigate_animation.dart';
import 'transitions_builder_manager.dart';

abstract class PageRouteBuilderManager {
  static build(
          {required Widget page, NavigateAnimation? navigateAnimation}) =>
      PageRouteBuilder(
          pageBuilder: (context, animation, secondaryAnimation) => page,
          transitionsBuilder: (context, animation, secondaryAnimation, child) =>
              navigateAnimation == NavigateAnimation.SlideHorizontal
                  ? TransitionsBuilderManager.slideHorizontal(
                      context, animation, secondaryAnimation, child)
                  : navigateAnimation == NavigateAnimation.SlideVertical
                      ? TransitionsBuilderManager.slideVertical(
                          context, animation, secondaryAnimation, child)
                      : TransitionsBuilderManager.fade(
                          context, animation, secondaryAnimation, child));
}
