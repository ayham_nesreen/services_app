import 'package:flutter/material.dart';

import 'transitions_builder_manager.dart';
import '../../enums/navigate_animation.dart';

abstract class NavigateManager {
  static Future<dynamic> push(
      {required BuildContext context,
      required Widget page,
      NavigateAnimation? navigateAnimation}) async {
    Navigator.push(
      context,
      (navigateAnimation == null)
          ? MaterialPageRoute(builder: (context) => page)
          : PageRouteBuilder(
              pageBuilder: (context, animation, secondaryAnimation) => page,
              transitionsBuilder: (context, animation, secondaryAnimation,
                      child) =>
                  navigateAnimation == NavigateAnimation.SlideHorizontal
                      ? TransitionsBuilderManager.slideHorizontal(
                          context, animation, secondaryAnimation, child)
                      : navigateAnimation == NavigateAnimation.SlideVertical
                          ? TransitionsBuilderManager.slideVertical(
                              context, animation, secondaryAnimation, child)
                          : TransitionsBuilderManager.fade(
                              context, animation, secondaryAnimation, child)),
    ).then((value) {
      return value;
    });
  }

  static Future<dynamic> pushReplacment(
      {required BuildContext context,
      required Widget page,
      NavigateAnimation? navigateAnimation}) async {
    Navigator.pushReplacement(
      context,
      (navigateAnimation == null)
          ? MaterialPageRoute(builder: (context) => page)
          : PageRouteBuilder(
              pageBuilder: (context, animation, secondaryAnimation) => page,
              transitionsBuilder: (context, animation, secondaryAnimation,
                      child) =>
                  navigateAnimation == NavigateAnimation.SlideHorizontal
                      ? TransitionsBuilderManager.slideHorizontal(
                          context, animation, secondaryAnimation, child)
                      : navigateAnimation == NavigateAnimation.SlideVertical
                          ? TransitionsBuilderManager.slideVertical(
                              context, animation, secondaryAnimation, child)
                          : TransitionsBuilderManager.fade(
                              context, animation, secondaryAnimation, child)),
    ).then((value) {
      return value;
    });
  }
}
