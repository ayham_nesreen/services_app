import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:services_app/core/ui/pages/splash_screen.dart';

import '../../../di/auth_di.dart';
import '../../../di/notifications_di.dart';
import '../../../features/auth/presentation/pages/login_page.dart';
import '../../../features/notifications/presentation/pages/notifications_page.dart';
import '../../../features/services_management/presentation/pages/add_service_page.dart';
import '../../../features/services_management/presentation/pages/services_page.dart';
import '../../resources/error_messages_manager.dart';
import '../../ui/pages/home_page.dart';
import '../basic/page_route_builder_manager.dart';
import 'routes_manager.dart';

class RoutesGenerator {
  static Route<dynamic> getRoute(RouteSettings settings) {
    switch (settings.name) {
      case RoutesManager.splash:
        initAuth();
        return PageRouteBuilderManager.build(page: const SplashScreen());

      case RoutesManager.login:
        initAuth();
        return PageRouteBuilderManager.build(page: const LoginPage());

      case RoutesManager.home:
        return PageRouteBuilderManager.build(page: const HomePage());

      case RoutesManager.notifications:
        initNotifications();
        return PageRouteBuilderManager.build(page: const NotificationsPage());

      case RoutesManager.addService:
        return PageRouteBuilderManager.build(page: const AddServicePage());

      case RoutesManager.services:
        return PageRouteBuilderManager.build(page: const ServicesPage());

      default:
        return unDefinedRoute();
    }
  }

  static Route<dynamic> unDefinedRoute() {
    return MaterialPageRoute(
        builder: (_) => Scaffold(
            appBar: AppBar(title: Text(ErrorMsgManager.routeNotFound.tr())),
            body: Text(ErrorMsgManager.routeNotFound.tr())));
  }
}
