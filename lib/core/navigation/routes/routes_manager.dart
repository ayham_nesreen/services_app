class RoutesManager {
  static const String splash = '/';
  static const String login = '/login';
  static const String home = '/home';
  static const String notifications = '/home/notifications';
  static const String addService = '/home/add-service';
  static const String services = '/home/services';
}
