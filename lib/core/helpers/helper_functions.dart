import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';

import '../structure_utils/errors/exceptions.dart';
import '../structure_utils/errors/failures.dart';
import '../structure_utils/network/network_info.dart';

abstract class HelperFunctions {
  ///Usage
  ///-----
  ///this function used to try to excute request and handle all types of Failure,
  ///it take generic type `<ReturnType>` to indicate the type of returned value
  ///_________
  ///Parmas
  ///------
  ///- `execute` this function indicate what you want to execute,
  ///it return Future of generic type.

  static Future<Either<Failure, ReturnType>> tryToExecute<ReturnType>({
    required NetworkInfo networkInfo,
    required Future<ReturnType> Function() execute,
  }) async {
    if (await networkInfo.isConnected()) {
      try {
        final result = await execute();
        return Right(result);
      } on DataException catch (e) {
        return Left(DataFailure(error: e.error));
      } on ConnectionException catch (_) {
        return Left(ConnectionFailure());
      } on TimeoutException catch (_) {
        return Left(TimeoutFailure());
      } on BadResponseException catch (_) {
        return Left(BadResponseFailure());
      } on RequestCancelledException catch (_) {
        return Left(RequestCancelledFailure());
      } on BadCertificateException catch (_) {
        return Left(BadCertificateFailure());
      } on UnknownException catch (_) {
        return Left(UnknownFailure());
      }
    } else {
      return Left(ConnectionFailure());
    }
  }

  static addFileToFormData({
    required FormData formData,
    required String key,
    required String path,
  }) async {
    formData.files.add(MapEntry(key, await MultipartFile.fromFile(path)));
  }

  static addFilesToFormData({
    required FormData formData,
    required String key,
    required List<String> paths,
  }) async {
    for (var item in paths) {
      formData.files.add(MapEntry(key, await MultipartFile.fromFile(item)));
    }
  }
}
