import 'package:flutter/material.dart';
import 'package:services_app/core/ui/theme/border_radius_manager.dart';

import '../theme/colors_manager.dart';
import '../theme/text_style_manager.dart';

abstract class SnackBarManager {
  static showSnackBar({required BuildContext context, required String msg}) {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(
          msg,
          style:
              TextStyleManager.f14w600.copyWith(color: ColorsManager.primary),
        ),
        duration: const Duration(milliseconds: 1000),
        backgroundColor: Colors.white,
        behavior: SnackBarBehavior.floating,
        margin: const EdgeInsets.only(bottom: 14, left: 16, right: 16),
        padding: const EdgeInsets.all(16),
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadiusManager.circular18),
      ),
    );
  }
}
