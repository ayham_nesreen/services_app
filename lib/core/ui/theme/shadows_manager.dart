import 'package:flutter/material.dart';

import 'colors_manager.dart';

abstract class ShadowsManager {
  static BoxShadow dropShadowGray10 = BoxShadow(
      offset: const Offset(3, -2),
      blurRadius: 6,
      spreadRadius: 0,
      color: ColorsManager.gray10);

  static BoxShadow checkboxShadow =
      BoxShadow(blurRadius: 5, spreadRadius: 0, color: ColorsManager.gray30);
}
