import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'border_radius_manager.dart';
import 'colors_manager.dart';
import 'text_style_manager.dart';

abstract class ThemeManager {
  //App Theme
  static ThemeData getTheme() {
    return ThemeData(
      useMaterial3: true,
      colorSchemeSeed: ColorsManager.primary,
      scaffoldBackgroundColor: ColorsManager.backgroundColor,
      appBarTheme: getAppBarTheme(),
      inputDecorationTheme: getInputDecorationTheme(),
      filledButtonTheme: getFilledButtonTheme(),
      textButtonTheme: getTextButtonTheme(),
      tabBarTheme: getTabBarTheme(),
      progressIndicatorTheme: getProgressIndicatorThemeData(),
    );
  }

  //AppBar Theme
  static getAppBarTheme() {
    return AppBarTheme(
      toolbarHeight: 100,
      shape: const RoundedRectangleBorder(
          borderRadius: BorderRadiusManager.onlyBottom45),
      systemOverlayStyle: const SystemUiOverlayStyle(
          statusBarIconBrightness: Brightness.light,
          statusBarColor: ColorsManager.primary),
      backgroundColor: ColorsManager.primary,
      centerTitle: false,
      elevation: 0,
      scrolledUnderElevation: 0,
      titleTextStyle: TextStyleManager.f24w400.copyWith(color: Colors.white),
    );
  }

  //TextField Decoration
  static InputDecorationTheme getInputDecorationTheme() {
    return InputDecorationTheme(
      contentPadding: const EdgeInsets.all(16),
      prefixIconColor: ColorsManager.primary,
      hintStyle: TextStyleManager.f16w400.copyWith(color: ColorsManager.gray),
      errorStyle: TextStyleManager.f13w400.copyWith(color: ColorsManager.red),
      errorMaxLines: 4,
      suffixIconColor: ColorsManager.gray,
      enabledBorder: const OutlineInputBorder(
        borderSide: BorderSide.none,
        borderRadius: BorderRadiusManager.circular18,
      ),
      focusedBorder: const OutlineInputBorder(
        borderSide: BorderSide.none,
        borderRadius: BorderRadiusManager.circular18,
      ),
      errorBorder: const OutlineInputBorder(
        borderSide: BorderSide.none,
        borderRadius: BorderRadiusManager.circular18,
      ),
      focusedErrorBorder: const OutlineInputBorder(
        borderSide: BorderSide.none,
        borderRadius: BorderRadiusManager.circular18,
      ),
    );
  }

  //Filled Button Theme
  static FilledButtonThemeData getFilledButtonTheme() {
    return const FilledButtonThemeData(
      style: ButtonStyle(
        shape: MaterialStatePropertyAll(RoundedRectangleBorder(
            borderRadius: BorderRadiusManager.circular18)),
        backgroundColor: MaterialStatePropertyAll(ColorsManager.primary),
        foregroundColor: MaterialStatePropertyAll(Colors.white),
        padding: MaterialStatePropertyAll(
            EdgeInsets.symmetric(horizontal: 16, vertical: 16)),
        textStyle: MaterialStatePropertyAll(TextStyleManager.f16w600),
      ),
    );
  }

  //Text Button Theme
  static TextButtonThemeData getTextButtonTheme() {
    return TextButtonThemeData(
      style: ButtonStyle(
          alignment: Alignment.center,
          padding: const MaterialStatePropertyAll(
              EdgeInsets.symmetric(horizontal: 16)),
          overlayColor:
              MaterialStatePropertyAll(ColorsManager.primary.withOpacity(0.07)),
          textStyle: MaterialStatePropertyAll(
              const TextStyle().copyWith(color: ColorsManager.primary))

          // ),
          ),
    );
  }

  static TabBarTheme getTabBarTheme() {
    return const TabBarTheme(
        labelColor: Colors.white,
        unselectedLabelColor: Colors.white60,
        overlayColor: MaterialStatePropertyAll(Colors.transparent),
        dividerColor: Colors.transparent,
        indicatorColor: Colors.transparent,
        labelPadding: EdgeInsets.symmetric(horizontal: 30),
        tabAlignment: TabAlignment.center);
  }

  //Progress Indicator Theme
  static getProgressIndicatorThemeData() {
    return ProgressIndicatorThemeData(
      circularTrackColor: ColorsManager.gray30,
      color: ColorsManager.primary,
    );
  }
}
