import 'package:flutter/material.dart';

import 'border_radius_manager.dart';
import 'shadows_manager.dart';

abstract class BoxDecorationManager {
  static BoxDecoration whiteBox = BoxDecoration(
      color: Colors.white,
      borderRadius: BorderRadiusManager.circular15,
      boxShadow: [ShadowsManager.dropShadowGray10]);
}
