import 'package:flutter/material.dart';

abstract class ColorsManager {
  static const Color primary = Color(0xff3b85fe);
  static const Color secondary = Color(0xff458cfe);

  static const Color backgroundColor = Color(0xfff5f6fa);

  static const Color red = Color(0xffFF3B30);

  static const Color gray = Color(0xffa5afb8);
  static const Color lightGray = Color(0xffF2F2F7);

  static Color gray10 = const Color(0xff828282).withOpacity(0.10);

  static Color gray30 = const Color(0xff828282).withOpacity(0.30);
}
