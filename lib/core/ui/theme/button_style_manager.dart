import 'package:flutter/material.dart';

import 'colors_manager.dart';

abstract class ButtonStyleManager {
  static ButtonStyle filledIconButton = const ButtonStyle().copyWith(
      maximumSize: const MaterialStatePropertyAll(Size(30, 30)),
      minimumSize: const MaterialStatePropertyAll(Size(30, 30)),
      padding: const MaterialStatePropertyAll(EdgeInsets.zero),
      backgroundColor: const MaterialStatePropertyAll(ColorsManager.primary));
}
