import 'package:flutter/material.dart';

abstract class TextStyleManager {
  static const TextStyle f11w400 =
      TextStyle(fontSize: 11, fontWeight: FontWeight.w400);

  static const TextStyle f13w400 =
      TextStyle(fontSize: 13, fontWeight: FontWeight.w400);

  static const TextStyle f14w400 =
      TextStyle(fontSize: 14, fontWeight: FontWeight.w400);

  static const TextStyle f14w600 =
      TextStyle(fontSize: 14, fontWeight: FontWeight.w600);

  static const TextStyle f16w400 =
      TextStyle(fontSize: 16, fontWeight: FontWeight.w400);

  static const TextStyle f16w500 =
      TextStyle(fontSize: 16, fontWeight: FontWeight.w500);

  static const TextStyle f16w600 =
      TextStyle(fontSize: 16, fontWeight: FontWeight.w600);

  static const TextStyle f24w400 =
      TextStyle(fontSize: 24, fontWeight: FontWeight.w400);
}
