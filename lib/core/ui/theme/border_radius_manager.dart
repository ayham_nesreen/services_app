import 'package:flutter/material.dart';

abstract class BorderRadiusManager {
  static const BorderRadius circular5 = BorderRadius.all(Radius.circular(5));
  static const BorderRadius circular15 = BorderRadius.all(Radius.circular(15));
  static const BorderRadius circular18 = BorderRadius.all(Radius.circular(18));
  static const BorderRadius circular32 = BorderRadius.all(Radius.circular(32));

  static const BorderRadius circularFull =
      BorderRadius.all(Radius.circular(100));

  static const BorderRadiusGeometry onlyStart15 = BorderRadiusDirectional.only(
      bottomStart: Radius.circular(15), topStart: Radius.circular(15));

  static const BorderRadiusGeometry onlyTopStart25 =
      BorderRadiusDirectional.only(topStart: Radius.circular(25));

  static const BorderRadiusGeometry onlyBottom45 = BorderRadiusDirectional.only(
    bottomEnd: Radius.circular(45),
    bottomStart: Radius.circular(45),
  );
}
