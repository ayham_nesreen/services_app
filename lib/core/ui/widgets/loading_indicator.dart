import 'package:flutter/material.dart';

class LoadingIndicator extends StatelessWidget {
  final EdgeInsetsGeometry padding;
  const LoadingIndicator({
    super.key,
    this.padding = EdgeInsets.zero,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      padding: padding,
      child: const CircularProgressIndicator(),
    );
  }
}
