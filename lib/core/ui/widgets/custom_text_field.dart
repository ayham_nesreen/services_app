import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../../validators/base_validator.dart';
import '../theme/border_radius_manager.dart';
import '../theme/colors_manager.dart';
import '../theme/shadows_manager.dart';

class CustomTextField extends StatefulWidget {
  final TextEditingController controller;
  final String? hint;
  final List<BaseValidator> validators;
  final bool isPassword;
  final TextInputType? keyboardType;
  final TextInputAction? textInputAction;
  final EdgeInsetsGeometry? margin;
  final String? prefixIconPath;

  const CustomTextField(
      {required this.controller,
      this.hint,
      this.keyboardType,
      this.textInputAction,
      this.isPassword = false,
      this.validators = const [],
      this.margin = const EdgeInsets.symmetric(horizontal: 36),
      this.prefixIconPath,
      super.key});

  @override
  CustomTextFieldState createState() => CustomTextFieldState();
}

class CustomTextFieldState extends State<CustomTextField> {
  final _formFieldKey = GlobalKey<FormFieldState<String>>();
  bool visibility = true;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: widget.margin,
      decoration: BoxDecoration(
          borderRadius: BorderRadiusManager.circular18,
          boxShadow: [ShadowsManager.dropShadowGray10]),
      child: TextFormField(
        key: _formFieldKey,
        textAlign: TextAlign.start,
        controller: widget.controller,
        autofocus: false,
        keyboardType: widget.isPassword
            ? TextInputType.visiblePassword
            : widget.keyboardType,
        obscureText: widget.isPassword ? visibility : false,
        cursorColor: ColorsManager.primary,
        textInputAction: widget.textInputAction ?? TextInputAction.done,
        // style: TextStyleManager.f16w400,
        decoration: const InputDecoration().copyWith(
          fillColor: Colors.white,
          filled: true,
          hintText: widget.hint ?? '',
          prefixIcon: widget.prefixIconPath != null
              ? Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: SvgPicture.asset(widget.prefixIconPath!,
                      color: ColorsManager.primary),
                )
              : null,
          suffixIcon: widget.isPassword
              ? IconButton(
                  icon: Icon(
                    visibility ? Icons.visibility : Icons.visibility_off,
                  ),
                  onPressed: () {
                    visibility = !visibility;
                    setState(() {});
                  },
                )
              : null,
        ),
        validator: (value) {
          return BaseValidator.validateValue(
            value!,
            [...widget.validators],
            true,
          );
        },
        onChanged: (value) {
          _formFieldKey.currentState!.validate();
        },
      ),
    );
  }
}
