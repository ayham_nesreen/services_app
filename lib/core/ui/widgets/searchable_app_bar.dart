import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:services_app/core/helpers/screen_manager.dart';
import 'package:services_app/core/ui/theme/border_radius_manager.dart';
import 'package:services_app/core/ui/theme/colors_manager.dart';
import 'package:services_app/core/ui/widgets/search_box.dart';

import '../../resources/svg_manager.dart';
import '../theme/text_style_manager.dart';

class SearchableAppBar extends StatelessWidget {
  final String title;
  final Function(String query) onSearch;
  const SearchableAppBar({
    super.key,
    required this.title,
    required this.onSearch,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 150,
      margin: const EdgeInsets.only(bottom: 10),
      child: Stack(
        children: [
          Container(
            width: ScreenManager.height,
            height: 125,
            padding: const EdgeInsetsDirectional.only(start: 24, end: 4),
            decoration: const BoxDecoration(
              color: ColorsManager.primary,
              borderRadius: BorderRadiusManager.onlyBottom45,
            ),
            child:
                Row(crossAxisAlignment: CrossAxisAlignment.center, children: [
              Expanded(
                  child: Text(title,
                      style: TextStyleManager.f24w400
                          .copyWith(color: Colors.white))),
              IconButton(
                  onPressed: () => Navigator.of(context).pop(),
                  icon: SvgPicture.asset(
                    SvgManager.arrowBack,
                    color: Colors.white,
                  )),
            ]),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: SearchBox(
              onSearch: onSearch,
            ),
          )
        ],
      ),
    );
  }
}
