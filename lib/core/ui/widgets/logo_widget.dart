import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../../helpers/screen_manager.dart';
import '../../resources/svg_manager.dart';

class LogoWidget extends StatelessWidget {
  const LogoWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(
          top: ScreenManager.fromHeight(13),
          bottom: ScreenManager.fromHeight(10)),
      child: SvgPicture.asset(SvgManager.logo),
    );
  }
}
