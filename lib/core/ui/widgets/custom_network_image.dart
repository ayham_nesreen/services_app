import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import '../../helpers/screen_manager.dart';
import '../theme/border_radius_manager.dart';
import '../theme/colors_manager.dart';
import 'skeleton_widget.dart';

class CustomNetworkImage extends StatelessWidget {
  final String imageUrl;
  final double? height;
  final double? width;
  final BoxFit fit;
  final BorderRadiusGeometry borderRadius;
  const CustomNetworkImage(
      {super.key,
      required this.imageUrl,
      this.height,
      this.width,
      this.fit = BoxFit.cover,
      this.borderRadius = BorderRadiusManager.circularFull});

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
        borderRadius: borderRadius,
        child: CachedNetworkImage(
          imageUrl: imageUrl,
          width: width ?? ScreenManager.fromWidth(20),
          height: height ?? ScreenManager.fromWidth(20),
          fit: fit,
          placeholder: (context, url) => SkeletonWidget(
            width: width ?? ScreenManager.fromWidth(20),
            height: height ?? ScreenManager.fromWidth(20),
            decoration: BoxDecoration(
              color: ColorsManager.lightGray,
              borderRadius: borderRadius,
            ),
          ),
          errorWidget: (context, url, error) => ClipRRect(
              borderRadius: borderRadius,
              child: Container(
                width: width ?? ScreenManager.fromWidth(20),
                height: height ?? ScreenManager.fromWidth(20),
                decoration: BoxDecoration(
                    borderRadius: borderRadius,
                    color: ColorsManager.primary.withOpacity(0.7)),
              )),
        ));
  }
}
