import 'package:flutter/material.dart';

import 'skeleton_widget.dart';

class TextSkeleton extends StatelessWidget {
  final double width;
  final double? height;
  const TextSkeleton({
    super.key,
    required this.width,
    this.height,
  });

  @override
  Widget build(BuildContext context) {
    return SkeletonWidget(width: width, height: height ?? 15);
  }
}
