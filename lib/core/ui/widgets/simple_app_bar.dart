import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../theme/colors_manager.dart';

class SimpleAppBar extends StatefulWidget implements PreferredSizeWidget {
  final String title;

  const SimpleAppBar({super.key, required this.title});
  @override
  SimpleAppBarState createState() => SimpleAppBarState();

  @override
  Size get preferredSize => const Size.fromHeight(60);
}

class SimpleAppBarState extends State<SimpleAppBar> {
  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: ColorsManager.backgroundColor,
      systemOverlayStyle: const SystemUiOverlayStyle(
          statusBarIconBrightness: Brightness.dark,
          statusBarColor: ColorsManager.backgroundColor),
      title: Text(widget.title,
          style: const TextStyle().copyWith(color: ColorsManager.primary)),
    );
  }
}
