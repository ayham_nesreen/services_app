import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../../resources/svg_manager.dart';

class BaseAppBar extends StatefulWidget implements PreferredSizeWidget {
  final String title;
  final List<Widget> actions;
  final PreferredSizeWidget? bottom;
  const BaseAppBar({
    super.key,
    required this.title,
    this.actions = const [],
    this.bottom,
  });
  @override
  BaseAppBarState createState() => BaseAppBarState();

  @override
  Size get preferredSize => const Size.fromHeight(100);
}

class BaseAppBarState extends State<BaseAppBar> {
  @override
  Widget build(BuildContext context) {
    return AppBar(
      leadingWidth: 0,
      leading: const SizedBox.shrink(),
      bottom: widget.bottom,
      title: Text(widget.title),
      actions: [
        ...widget.actions,
        IconButton(
            onPressed: () => Navigator.of(context).pop(),
            icon: SvgPicture.asset(
              SvgManager.arrowBack,
              color: Colors.white,
            )),
      ],
    );
  }
}
