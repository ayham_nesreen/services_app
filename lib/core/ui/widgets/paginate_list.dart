import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

import '../../resources/strings_manager.dart';
import 'loading_indicator.dart';

class PaginateList<T> extends StatelessWidget {
  final int skip;
  final int total;

  ///Indicate if there is request right now or not
  final bool isLoading;
  final List<T> items;

  ///Here, in this function you have to call API on next page
  final Function() call;
  final Widget Function(T) itemBuilder;
  final Widget? skeletonWidget;

  const PaginateList({
    super.key,
    required this.skip,
    required this.total,
    required this.isLoading,
    required this.items,
    required this.call,
    required this.itemBuilder,
    this.skeletonWidget,
  });

  @override
  Widget build(BuildContext context) {
    return (isLoading && skip == 0)
        ? (skeletonWidget != null)
            ? ListView.builder(
                itemCount: 4, itemBuilder: (_, index) => skeletonWidget)
            : const LoadingIndicator()
        : (items.isEmpty)
            ? Center(child: Text(StringsManager.noResult.tr()))
            : ListView.builder(
                padding: const EdgeInsets.only(bottom: 100),
                itemCount: isLoading ? items.length + 1 : items.length,
                itemBuilder: (_, index) {
                  if (index == items.length - 1 && !isLoading) {
                    if (skip < total) {
                      call();
                    }
                  }
                  return (index == items.length && isLoading)
                      ? skeletonWidget ?? const LoadingIndicator()
                      : itemBuilder(items[index]);
                },
              );
  }
}
