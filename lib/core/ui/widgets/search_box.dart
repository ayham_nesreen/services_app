import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

import '../../resources/strings_manager.dart';
import '../theme/border_radius_manager.dart';
import '../theme/colors_manager.dart';

class SearchBox extends StatefulWidget {
  final Function(String query) onSearch;

  const SearchBox({super.key, required this.onSearch});

  @override
  SearchBoxState createState() => SearchBoxState();
}

class SearchBoxState extends State<SearchBox> {
  TextEditingController controller = TextEditingController(text: '');

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 24),
      child: TextFormField(
        textAlign: TextAlign.start,
        controller: controller,
        onFieldSubmitted: (value) {
          widget.onSearch(value);
        },
        autofocus: false,
        cursorColor: ColorsManager.primary,
        textInputAction: TextInputAction.done,
        decoration: const InputDecoration().copyWith(
          enabledBorder: const OutlineInputBorder(
            borderSide: BorderSide(width: 1, color: ColorsManager.gray),
            borderRadius: BorderRadiusManager.circular32,
          ),
          focusedBorder: const OutlineInputBorder(
            borderSide: BorderSide(width: 1, color: ColorsManager.primary),
            borderRadius: BorderRadiusManager.circular32,
          ),
          errorBorder: const OutlineInputBorder(
            borderSide: BorderSide(width: 1, color: ColorsManager.red),
            borderRadius: BorderRadiusManager.circular32,
          ),
          focusedErrorBorder: const OutlineInputBorder(
            borderSide: BorderSide(width: 1, color: ColorsManager.red),
            borderRadius: BorderRadiusManager.circular32,
          ),
          border: OutlineInputBorder(
            borderSide: BorderSide(width: 1, color: ColorsManager.gray30),
            borderRadius: BorderRadiusManager.circular32,
          ),
          contentPadding:
              const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
          fillColor: Colors.white,
          filled: true,
          hintText: StringsManager.search.tr(),
          prefixIcon: IconButton(
            onPressed: () {
              widget.onSearch(controller.text);
            },
            icon: const Icon(Icons.search, color: ColorsManager.primary),
          ),
          suffixIcon: IconButton(
            icon: Icon(Icons.cancel_sharp, color: ColorsManager.gray30),
            onPressed: () {
              setState(() {
                controller.clear();
              });
            },
          ),
        ),
      ),
    );
  }
}
