import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

import '../theme/colors_manager.dart';

class SkeletonWidget extends StatelessWidget {
  final double height;
  final double width;
  final BoxDecoration? decoration;
  final EdgeInsetsGeometry? margin;
  final EdgeInsetsGeometry? padding;
  const SkeletonWidget({
    super.key,
    required this.height,
    required this.width,
    this.decoration,
    this.margin,
    this.padding,
  });

  @override
  Widget build(BuildContext context) {
    return Shimmer.fromColors(
        baseColor: ColorsManager.gray30,
        highlightColor: Colors.white,
        child: Container(
          height: height,
          width: width,
          padding: padding,
          margin: margin,
          decoration: decoration ??
              BoxDecoration(
                  color: ColorsManager.gray30,
                  borderRadius: BorderRadius.circular(12)),
        ));
  }
}
