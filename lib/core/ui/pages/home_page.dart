import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

import '../../navigation/routes/routes_manager.dart';
import '../../resources/strings_manager.dart';
import '../widgets/simple_app_bar.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: SimpleAppBar(title: StringsManager.home.tr()),
      body: Center(
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              FilledButton(
                child: Text(StringsManager.viewServices.tr()),
                onPressed: () =>
                    Navigator.of(context).pushNamed(RoutesManager.services),
              ),
              FilledButton(
                child: Text(StringsManager.notifications.tr()),
                onPressed: () => Navigator.of(context)
                    .pushNamed(RoutesManager.notifications),
              )
            ]),
      ),
    );
  }
}
