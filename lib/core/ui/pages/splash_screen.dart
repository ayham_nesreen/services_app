import 'package:flutter/material.dart';

import '../../../di/injection_container.dart';
import '../../../features/auth/data/models/user_model.dart';
import '../widgets/logo_widget.dart';
import '../../navigation/routes/routes_manager.dart';
import '../../preferences/app_preferences.dart';
import '../theme/colors_manager.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({super.key});

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  AppPreferences preferences = instance<AppPreferences>();

  UserModel? userModel;

  @override
  void initState() {
    userModel = preferences.getUser();
    Future.delayed(const Duration(milliseconds: 1000)).then((value) {
      if (userModel == null) {
        Navigator.of(context).pushReplacementNamed(RoutesManager.login);
      } else {
        Navigator.of(context).pushReplacementNamed(RoutesManager.home);
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          color: ColorsManager.backgroundColor,
          alignment: Alignment.center,
          height: double.infinity,
          width: double.infinity,
          child: const LogoWidget()),
    );
  }
}
