abstract class SvgManager {
  static const String baseUrl = 'assets/svg/';

  static const String arrowBack = '${baseUrl}arrow_back.svg';
  static const String arrowDown = '${baseUrl}arrow_down.svg';
  static const String arrowRegister = '${baseUrl}arrow_register.svg';
  static const String cancel = '${baseUrl}cancel.svg';
  static const String logo = '${baseUrl}logo.svg';
  static const String password = '${baseUrl}password.svg';
  static const String showPassword = '${baseUrl}show_password.svg';
  static const String user = '${baseUrl}user.svg';
  static const String username = '${baseUrl}username.svg';
  static const String viewPassword = '${baseUrl}view_password.svg';
}
