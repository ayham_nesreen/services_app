abstract class StringsManager {
  static const String appName = 'app_name';
  static const String login = 'login';
  static const String username = 'username';
  static const String password = 'password';
  static const String forgetPassword = 'forget_password';
  static const String privacyPolicyTerms = 'privacy_policy_terms';
  static const String privacyPolicyTermsPrefix = 'privacy_policy_terms_prefix';
  static const String register = 'register';
  static const String guest = 'guest';
  static const String dontHaveAccount = 'dont_have_account';
  static const String notifications = 'notifications';
  static const String dummyClinicName = 'dummy_clinic_name';
  static const String addService = 'add_service';
  static const String viewServices = 'view_services';
  static const String home = 'home';
  static const String showMyTasks = 'show_my_tasks';
  static const String services = 'services';
  static const String servicesDescription = 'services_description';
  static const String linkNewService = 'link_new_service';
  static const String addNewServices = 'add_new_services';
  static const String search = 'search';
  static const String done = 'done';
  static const String noResult = 'no_result';
}
