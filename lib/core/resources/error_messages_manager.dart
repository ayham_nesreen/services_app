abstract class ErrorMsgManager {
  static const String timeoutError = 'timeout_error';
  static const String requestCancelled = 'request_cancelled';
  static const String badResponse = 'bad_response';
  static const String connectionError = 'connection_error';
  static const String badCertificate = 'bad_certificate';
  static const String unexcpectedError = 'unexcpected_error';
  static const String unknownError = 'unknown_error';

  static const String routeNotFound = 'route_not_found';
  static const String fieldRequired = 'field_required';
}
