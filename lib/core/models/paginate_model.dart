class PaginateModel<T> {
  List<T> items;
  int total;
  int skip;
  int limit;

  PaginateModel({
    this.items = const [],
    this.total = 0,
    this.skip = 0,
    this.limit = 10,
  });

  factory PaginateModel.fromJson(Map<String, dynamic> json, List<dynamic> list,
      T Function(dynamic json) fromJsonT) {
    return PaginateModel(
      items: list.map((item) => fromJsonT(item)).toList(),
      total: json['total'],
      skip: json['skip'],
      limit: json['limit'],
    );
  }
}
